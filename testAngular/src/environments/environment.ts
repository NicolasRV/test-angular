// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};
export var firebaseConfig = {
  apiKey: "AIzaSyDFBHeHjVz-45TrpwY0AkncBNjsAhgnusE",
  authDomain: "test-angular-82f3d.firebaseapp.com",
  databaseURL: "https://test-angular-82f3d.firebaseio.com",
  projectId: "test-angular-82f3d",
  storageBucket: "test-angular-82f3d.appspot.com",
  messagingSenderId: "107722106077",
  appId: "1:107722106077:web:5a18627a808836fa"
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
