import { TestBed } from '@angular/core/testing';
import { AngularFirestore } from '@angular/fire/firestore';
import { DataTableModule } from "angular2-datatable";
import { firebaseConfig, environment } from '../../environments/environment';
import { AngularFireModule } from "@angular/fire";
import { ConexionService } from './conexion.service';

describe('ConexionService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [DataTableModule, AngularFireModule.initializeApp(firebaseConfig)],
    providers: [AngularFirestore]
  }));

  it('should be created', () => {
    const service: ConexionService = TestBed.get(ConexionService);
    expect(service).toBeTruthy();
  });
});
