import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class ConsultaHttpService {
  url: any;

  constructor(private Http: Http) {
    this.url = "https://randomuser.me/api/";
  }

  getData() {
    return this.Http.get(this.url, this.jwt()).pipe(map((res: Response) => res.json()));
  }

  private jwt() {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return new RequestOptions({ headers: headers });
  }
}
