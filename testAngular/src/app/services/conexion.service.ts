import { Injectable, Output, EventEmitter } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ConexionService {

  constructor(private firestore: AngularFirestore) { }

  public createPatient(data: { name: string, email: any, age: number, idNumber: number, gender: string, cellphone: number }) {
    return this.firestore.collection('createPatient').add(data);
  }
  public get() {
    return this.firestore.collection('createPatient').snapshotChanges();
  }
}
