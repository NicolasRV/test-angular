import { TestBed } from '@angular/core/testing';
import { HttpModule } from '@angular/http';
import { ConsultaHttpService } from './consulta-http.service';

describe('ConsultaHttpService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpModule]
  }));

  it('should be created', () => {
    const service: ConsultaHttpService = TestBed.get(ConsultaHttpService);
    expect(service).toBeTruthy();
  });
});
