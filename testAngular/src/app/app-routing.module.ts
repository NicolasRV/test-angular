import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { CreatePatientComponent } from './components/create-patient/create-patient.component';
import { TableHttpComponent } from './components/table-http/table-http.component';

const routes: Routes = [
  { path: '', component: HomeComponent, children:[
    { path: 'create-patient', component: CreatePatientComponent },
    { path: 'table-http', component: TableHttpComponent },
  ] },

  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
