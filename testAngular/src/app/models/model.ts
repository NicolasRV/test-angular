export class Model{
   constructor(
       public idNumber: number,
       public name: string,
       public email: string,
       public age: number,
       public gender: string,
       public cellphone: number
   ){ }
}