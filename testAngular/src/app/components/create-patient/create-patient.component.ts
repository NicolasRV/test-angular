import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormGroupDirective, NgForm, FormControl } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { ConexionService } from '../../services/conexion.service';
import { Model } from 'src/app/models/model';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}
@Component({
  selector: 'app-create-patient',
  templateUrl: './create-patient.component.html',
  styleUrls: ['./create-patient.component.sass']
})
export class CreatePatientComponent implements OnInit {
  title = 'Title @Input';
  patientFormGroup: FormGroup;
  patient: Model;
  numero: number;
  texto: string;

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  matcher = new MyErrorStateMatcher();

  constructor(fb: FormBuilder, private router: Router, private _formBuilder: FormBuilder, private conexion: ConexionService) {
    this.patient = new Model(this.numero, this.texto, this.texto, this.numero, this.texto, this.numero);
  }

  ngOnInit() {
    this.patientFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required],
      email: ['', [Validators.required,
      Validators.email,]]

    });
  }

  sendData() {
    document.getElementById('table').classList.add('table');
    let patientVar = {
      idNumber: this.patient.idNumber, age: this.patient.age, cellphone: this.patient.cellphone, email: this.patient.email, gender: this.patient.gender, name: this.patient.name
    }
    this.conexion.createPatient(patientVar).then(() => {
    })
  }
}
