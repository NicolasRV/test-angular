import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatSelectModule } from '@angular/material/select';
import { MatRadioModule } from '@angular/material/radio';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { AppRoutingModule } from '../../app-routing.module';
import { CdkTableModule } from '@angular/cdk/table';
import { TableHttpComponent } from '../../components/table-http/table-http.component';
import { DataTableModule } from "angular2-datatable";
import { HttpModule } from '@angular/http';
import { AngularFirestore } from '@angular/fire/firestore';
import { firebaseConfig, environment } from '../../../environments/environment';
import { ServiceWorkerModule } from '@angular/service-worker';
import { AngularFireModule } from "@angular/fire";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from '../../components/home/home.component';
import { CreatePatientComponent } from '../../components/create-patient/create-patient.component';
import { MainNavComponent } from './main-nav.component';
import { APP_BASE_HREF } from '@angular/common';

describe('MainNavComponent', () => {
  let component: MainNavComponent;
  let fixture: ComponentFixture<MainNavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule, FormsModule, ReactiveFormsModule, MatCardModule, MatSelectModule, MatRadioModule, MatFormFieldModule, MatInputModule, AppRoutingModule, CdkTableModule, DataTableModule, HttpModule, AngularFireModule.initializeApp(firebaseConfig), HttpModule, ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }), BrowserAnimationsModule
      ],
      declarations: [TableHttpComponent, MainNavComponent, HomeComponent, CreatePatientComponent],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
        NO_ERRORS_SCHEMA
      ],
      providers: [AngularFirestore, { provide: APP_BASE_HREF, useValue: '/' }]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
