import { Component, OnInit } from '@angular/core';
import { ConsultaHttpService } from 'src/app/services/consulta-http.service';

@Component({
  selector: 'app-table-http',
  templateUrl: './table-http.component.html',
  styleUrls: ['./table-http.component.sass']
})
export class TableHttpComponent implements OnInit {
  
  consulta: any;

  rowsOnPage = 5;
  sortBy = "position";
  sortOrder = "asc";

  constructor(private conexion: ConsultaHttpService) { }

  ngOnInit() {
    this.data();
  }

  data(){
    this.conexion.getData().subscribe(data=>{
      this.consulta = data.results;
    })
  }

}
