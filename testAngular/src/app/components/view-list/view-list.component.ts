import { Component, OnInit, Input } from '@angular/core';
import { ConexionService } from 'src/app/services/conexion.service';

export class modelList {
    public id: any;
    public idNumber: number;
    public name: string;
    public email: string;
    public age: number;
    public gender: string;
    public cellphone: number
}

@Component({
  selector: 'app-view-list',
  templateUrl: './view-list.component.html',
  styleUrls: ['./view-list.component.sass']
})
export class ViewListComponent implements OnInit {
  @Input () title:any;
  displayedColumns: string[] = ['name', 'age', 'email', 'gender'];
  dataSource: any;
  data: any;
  patientModel: modelList[] = [];

  constructor(private conxion: ConexionService) {

   }
  
  ngOnInit() {
    this.conxion.get().subscribe(patient => {
      this.patientModel = patient.map(e => {
        return {
          id: e.payload.doc.id,
          ...e.payload.doc.data()
        } as modelList;
      });
      this.dataSource = this.patientModel;
    })
  }

}
