import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DataTableModule } from "angular2-datatable";
import { CdkTableModule } from '@angular/cdk/table';
import { AngularFirestore } from '@angular/fire/firestore';
import { firebaseConfig, environment } from '../../../environments/environment';
import { AngularFireModule } from "@angular/fire";

import { ViewListComponent } from './view-list.component';

describe('ViewListComponent', () => {
  let component: ViewListComponent;
  let fixture: ComponentFixture<ViewListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [DataTableModule, CdkTableModule, AngularFireModule.initializeApp(firebaseConfig)],
      declarations: [ViewListComponent],
      providers: [AngularFirestore]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
