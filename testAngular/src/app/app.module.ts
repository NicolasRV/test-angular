import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainNavComponent } from './components/main-nav/main-nav.component';
import { HomeComponent } from './components/home/home.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatSelectModule } from '@angular/material/select';
import { MatRadioModule } from '@angular/material/radio';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { ViewListComponent } from './components/view-list/view-list.component';
import { AngularFireModule } from "@angular/fire";
import { firebaseConfig, environment } from '../environments/environment';
import { ConexionService } from './services/conexion.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { CdkTableModule } from '@angular/cdk/table';
import { CreatePatientComponent } from './components/create-patient/create-patient.component';
import { TableHttpComponent } from './components/table-http/table-http.component';
import { DataTableModule } from "angular2-datatable";
import { ConsultaHttpService } from './services/consulta-http.service';
import { HttpModule } from '@angular/http';
import { ServiceWorkerModule } from '@angular/service-worker';


@NgModule({
  declarations: [
    AppComponent,
    MainNavComponent,
    HomeComponent,
    ViewListComponent,
    CreatePatientComponent,
    TableHttpComponent
  ],
  imports: [
    DataTableModule,
    BrowserModule,
    AppRoutingModule,
    MatToolbarModule,
    MatButtonModule, 
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    BrowserAnimationsModule,
    FormsModule, 
    MatFormFieldModule,
    ReactiveFormsModule,
    MatCardModule,
    MatSelectModule,
    MatInputModule,
    MatRadioModule,
    AngularFireModule.initializeApp(firebaseConfig),
    CdkTableModule, 
    HttpModule, ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
    
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ],
  providers: [AngularFirestore, ConexionService, ConsultaHttpService],
  bootstrap: [AppComponent]
})
export class AppModule { }
